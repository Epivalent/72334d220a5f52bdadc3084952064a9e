Bonjour! En français, le mot "article" est masculin. Il n'y a pas de règle spécifique pour cela; c'est juste comme ça. Beaucoup de mots en français ont un genre (masculin ou féminin) sans raison logique.

Quelques exemples similaires :
- Le problème (masculin)
- Le programme (masculin)
- La table (féminin)

C'est important d'apprendre le genre des mots avec chaque nouveau mot que tu apprends. Bon courage!

# Wed 26 Jun 22:33:28 CEST 2024 - Hello. Why is article masculine in French? What's the rule here? Is this an exception? Are there other examples similarly exceptional to it? Answer in A2 CEFR graded French. Do not stray into B1 graded French; keep it très simple!